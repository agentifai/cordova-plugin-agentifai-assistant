#!/bin/sh

PUBLIC_REPO_URL=git@bitbucket.org:agentifai/cordova-plugin-agentifai-assistant.git
TEMP_FOLDER_NAME=deploy_temp

echo_argument_error() {
    echo "Error: Missing expected arguments"
    echo ""
    echo "USAGE: deploy <version> <update_master>"
    echo ""
    echo "ARGUMENTS:"
    echo "  <version> The release version"
    echo "  <update_master> The master branch should be updated or not"
    echo ""
}

copyFilesToPublish() {
  # Copy all files except some files
  rsync -av --progress --exclude=".*" --exclude README.md  --exclude LICENCE --exclude $TEMP_FOLDER_NAME --exclude deploy.sh $1 $2
}

createTempFolder() {
  rm -f -rf $1
  mkdir $1
}

deploy() {
  echo "Deploying $VERSION"

  BASEDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
  TEMP_DIR="$BASEDIR/$TEMP_FOLDER_NAME"
  
  createTempFolder $TEMP_DIR &&
  git clone $PUBLIC_REPO_URL $TEMP_DIR &&
  copyFilesToPublish . $TEMP_DIR &&
  cd $TEMP_DIR &&
  git checkout -b release/$VERSION &&
  git add -A &&
  git commit -am $VERSION &&
  
  if [ $UPDATE_MASTER = true ]; 
  then 
    git checkout master &&
    git merge release/$VERSION &&
    git push origin master
  fi

  git tag $VERSION &&
  git push --tags
  
  rm -rf $TEMP_DIR
  return $?
}

VERSION=$1 
UPDATE_MASTER=$2

if [ $# -eq 2 ]
then
  deploy
else
  echo_argument_error
fi