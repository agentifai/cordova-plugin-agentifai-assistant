const logger = require('../logger');
var utilities = require('../utilities');

// Must be the same defined in plugin.xml
const ANDROID_URL_KEY = 'AGENTIFAI_ASSISTANT_ANDROID_URL';
const ANDROID_VERSION_KEY = 'AGENTIFAI_ASSISTANT_ANDROID_VERSION';
const USERNAME = 'AGENTIFAI_ASSISTANT_USERNAME';
const PASSWORD = 'AGENTIFAI_ASSISTANT_PASSWORD';

function isValid(value) {
  return value !== null && value !== undefined && value !== '';
}

function addGradleDependency(context, url, version, username, password) {
  var gradle = utilities.getGradleFile(context).toString();
  gradle = gradle.replace(`${ANDROID_URL_KEY}`, url);
  gradle = gradle.replace(`${ANDROID_VERSION_KEY}`, version);

  if (isValid(username)) {
    gradle = gradle.replace(`${USERNAME}`, `'${username}'`);
  }
  
  if (isValid(password)) {
    gradle = gradle.replace(`${PASSWORD}`, `'${password}'`);
  }

  utilities.writeGradleFile(context, gradle);
}

module.exports = function(context) {
  logger.log('Adding Agentifai gradle dependency');
  
  const configFile = utilities.getConfigFile();
  const pluginFile = utilities.getPluginFile(context);

  //Get values from config.xml
  var url = utilities.getPreferenceValue(configFile, ANDROID_URL_KEY);
  var version = utilities.getPreferenceValue(configFile, ANDROID_VERSION_KEY);

  //Get variables from plugin installation command
  if (!isValid(url)) {
    url = utilities.getVariableValue(process.argv, ANDROID_URL_KEY);
  }

  if (!isValid(version)) {
    version = utilities.getVariableValue(process.argv, ANDROID_VERSION_KEY);
  }

  //Get default values from plugin.xml
  if (!isValid(version)) {
    version = utilities.getPreferenceValue(pluginFile, ANDROID_VERSION_KEY);
  }

  if(!isValid(url)) {
    throw new Error(`${ANDROID_URL_KEY} not found. Add ${ANDROID_URL_KEY} as plugin variable`);
  }

  if(!isValid(version)) {
    throw new Error(`${ANDROID_VERSION_KEY} not found. Add ${ANDROID_VERSION_KEY} as plugin variable`);
  }

  logger.log(`Android assistant URL: ${url}`);
  logger.log(`Android assistant version: ${version}`);

  //these preferences are optional, they can be added in the global gradle properties
  const username = utilities.getPreferenceValue(configFile, USERNAME);
  const password = utilities.getPreferenceValue(configFile, PASSWORD);

  addGradleDependency(context, url, version, username, password);
}