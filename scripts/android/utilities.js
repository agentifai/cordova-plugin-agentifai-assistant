const path = require('path');
const utilities = require('../utilities');

module.exports = {

  /**
   * Get android assets path
   */

  getAndroidAssetsPath: function(context) {
    return path.join(utilities.getRootPath(context),'platforms/android/app/src/main/assets');
  }

}