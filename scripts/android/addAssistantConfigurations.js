const logger = require("../logger");
const path = require('path');
const utilities = require('../android/utilities');
const configurationManager = require("../configuration/configurationManager");
const constants = require("../constants");

module.exports = function(context) {
    logger.log('Adding Assistant configurations');

    const configurationPlatformPath = path.join(utilities.getAndroidAssetsPath(context), constants.getConfigurationFolderName());

    configurationManager.copyConfiguration(context, configurationPlatformPath);
}