const logger = require("../logger");
const path = require('path');
const utilities = require("../utilities");
const androidUtilities = require('../android/utilities');
const constants = require("../constants");

module.exports = function(context) {
    logger.log('Removing Assistant configurations');

    const configurationPlatformPath = path.join(androidUtilities.getAndroidAssetsPath(context), constants.getConfigurationFolderName());
    utilities.removeDir(configurationPlatformPath);
}