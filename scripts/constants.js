
module.exports = {
  getConfigurationFolderName: function() {
    return 'agentifai'
  },

  getAnimationsConfigurationFolderName: function() {
    return 'animations'
  },

  getImagesConfigurationFolderName: function() {
    return 'images'
  },

  getFontsConfigurationFolderName: function() {
    return 'fonts'
  },

  getConfigurationZipFileName: function() {
    return 'Agentifai.zip'
  },

  getIosConfigurationFolderName: function() {
    return 'Agentifai'
  },

  getAcceptableConfigFileExtensions: function() {
    return [this.getConfigFileExtension(), 'ttf', 'pdf']
  },

  getConfigFileExtension: function() {
    return 'json'
  }
}