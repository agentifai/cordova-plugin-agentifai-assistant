const path = require('path');
const constants = require('../constants');
const utilities = require('../utilities');

class ConfigurationType {
  static Folder = new ConfigurationType("folder")
  static Zip = new ConfigurationType("zip")

  constructor(type) {
    this.type = type;
  }
}

class Configuration {
  constructor(type, path) {
    this.type = type;
    this.path = path;
  }
}

function getPossibleConfigurations(context) {
  return [
    new Configuration(
      ConfigurationType.Folder,
      path.join(utilities.getRootPath(context), constants.getConfigurationFolderName())
    ),
    new Configuration(
      ConfigurationType.Zip,
      path.join(utilities.getRootPath(context), constants.getConfigurationZipFileName())
    ),
    new Configuration(
      ConfigurationType.Folder,
      path.join(utilities.getRootPath(context), 'www', constants.getConfigurationFolderName())
    ),
    new Configuration(
      ConfigurationType.Zip,
      path.join(utilities.getRootPath(context), 'www', constants.getConfigurationZipFileName())
    )
  ]
}

function isConfigValid(config) {
  return config 
      && utilities.existsFile(config.path)
      && (config.type == ConfigurationType.Folder || config.type == ConfigurationType.Zip);
}

function copyConfiguration(config, destinationPath) {
  if (config.type == ConfigurationType.Folder) {
    return utilities.copyDirectory(config.path, destinationPath);
  } else {
    return utilities.unzip(config.path, destinationPath);
  }
}

function existsFileConfig(destinationPath) {
  return utilities.getOnlyFiles(destinationPath)
        .map(file => file.indexOf(constants.getConfigFileExtension()) >= 0 )
        .reduce((accumulator, currentValue) => accumulator || currentValue, false)
}

module.exports = { 
  copyConfiguration: function(context, destinationPath) {
    const configs = getPossibleConfigurations(context);
    var config;

    configs.forEach(conf => {
      if (isConfigValid(conf)) {
        config = conf;
      }
    })

    if (!config) {
      throw new Error('Agentifai configuration not found. Please add Agentifai configuration to the project.');
    }

    if (!utilities.existsFile(destinationPath)) {
      utilities.makeDir(destinationPath);
    }

    copyConfiguration(config, destinationPath);
    
    if (!existsFileConfig(destinationPath)) {
      throw new Error(`Configuration file not found. Please, check if the file exists in the Agentifai configuration.`);
    }
  }
}