const fs = require('fs');
const path = require('path');
const fsExtra = require('fs-extra');
const { spawnSync } = require('child_process');

module.exports = {
  /**
    * The ID of the plugin; this should match the ID in plugin.xml.
  */
  getPluginId: function(context) {
    return context.opts.plugin.id;
  },

  getPluginPath: function(context) {
    return context.opts.plugin.dir;
  },

  getConfigFile: function() {
    return this.readFile('config.xml').toString();
  },

  getPluginFile: function(context) {
    const pluginXmlPath = path.join(this.getPluginPath(context), 'plugin.xml');
    return this.readFile(pluginXmlPath);
  },

  getCordovaCommomModule: function(context) {
    return context.requireCordovaModule('cordova-common');
  },

  getConfigParser: function(context) {
    return this.getCordovaCommomModule(context).ConfigParser
  },

  /**
    * Used to get the preference value defined in config.xml or plugin.xml.
  */
  getPreferenceValue: function(config, name) {
    var value = config.match(new RegExp('name="' + name + '" (?:value|default)="(.*?)"', "i"))
    if(value && value[1]) {
        return value[1]
    } else {
        return null
    }
  },
  
  /**
    * Used to get the variable value defined in plugin installation.
  */
  getVariableValue: function(args, name) {
    for(var i in args) {
      var arg = args[i]
      var value = arg.match(new RegExp(name + '=(.*$)', "i"))
      if(value && value[1]) {
        return value[1]
      }
    }
  },

  /**
    * Used to get path of agentifai.gradle
  */
  getGradleFilePath: function(context) {
    return path.join(this.getPluginPath(context), 'src/android/agentifai.gradle');
  },

  /**
    * Used to get agentifai.gradle
  */
  getGradleFile: function(context) {
    const gradle = this.getGradleFilePath(context);
    return this.readFile(gradle);
  },

  /**
    * Used to write to agentifai.gradle
  */
  writeGradleFile: function(context, data) {
    const file = this.getGradleFilePath(context);
    this.writeFile(file, data);
  },

  /**
    * Write data to file
  */
  writeFile: function(path, data) {
    return fs.writeFileSync(path, data, 'utf8');
  },

  /**
    * Append data to file
  */
  appendFile: function(path, data) {
    return fs.appendFileSync(path, data, 'utf8');
  },
  

  /**
    * Read data from file
  */
  readFile: function(path) {
    return fs.readFileSync(path, 'utf8');
  },

  /**
    * Check if files exists
  */
  existsFile: function(path) {
    return fs.existsSync(path, 'utf8');
  },

  /**
    * Get only files present in path
  */
  getOnlyFiles: function(dir) {
    return fs.readdirSync(dir)
        .filter(content => {
            const contentPath = path.join(dir, content);
            return fs.lstatSync(contentPath).isFile();
        });
  },

  /**
    * Create a directory
  */
  makeDir: function(dir) {
    return fs.mkdirSync(dir, { recursive: true });
  },

  /**
    * Remove directory
  */
  removeDir: function(path) {
    if(this.existsFile(path)) {
      return fsExtra.removeSync(path);  
    }
  },

  /**
   * check if a path exists
  */
  existsDir: function(path) {
    return fs.existsSync(path);
  },

  /**
   * Get project root path
  */
  getRootPath: function(context){
    return context.opts.projectRoot;
  },

  /**
   * Copy directory to another destination
  */
  copyDirectory: function (source, destination) {
    fs.mkdirSync(destination, { recursive: true });
    
    fs.readdirSync(source, { withFileTypes: true }).forEach((entry) => {
      let sourcePath = path.join(source, entry.name);
      let destinationPath = path.join(destination, entry.name);

      entry.isDirectory()
        ? this.copyDirectory(sourcePath, destinationPath)
        : fs.copyFileSync(sourcePath, destinationPath);
    });
  },

  /**
   * Get app name
  */
  getAppName: function(context) {
    const ConfigParser = this.getConfigParser(context)
    
    const config = new ConfigParser('config.xml')
    return config.name()
  },

  /**
   * Unzip file
  */
  unzip: function(filepath, destinationPath) {
    const opts = {};
    const args = ['-o', filepath];
    if (destinationPath) {
      args.push(['-d', destinationPath]);
    }

    return spawnSync('unzip', ['-o', filepath, '-d', destinationPath], opts);
  }

}