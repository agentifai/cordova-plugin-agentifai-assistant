const logger = require("../logger");
const path = require('path');
const iosUtilities = require('./utilities');
const constants = require('../constants');
const fs = require('fs');
const xcode = require('xcode');
const configurationManager = require("../configuration/configurationManager");
const utilities = require("../utilities");

function getFiles(filespath) {
    return utilities.getOnlyFiles(filespath)
        .filter(content => isValidFileExtension(content));
}

function isValidFileExtension(file) {
    return constants.getAcceptableConfigFileExtensions()
        .map(extension => file.indexOf(extension) >= 0)
        .reduce((accumulator, currentValue) => accumulator || currentValue, false)
}

function addResourceFilesFromDir(dir, relativePath, project) {
    const files = getFiles(dir)
    files.forEach(file => {
        project.addResourceFile(path.join(relativePath, file));
    })
}

function copyConfigurationToProject(context, configurationPlatformPath) {
    const platformAnimationsPath = path.join(configurationPlatformPath, constants.getAnimationsConfigurationFolderName());
    const platformImagesPath = path.join(configurationPlatformPath, constants.getImagesConfigurationFolderName());
    const platformFontsPath = path.join(configurationPlatformPath, constants.getFontsConfigurationFolderName());
    const configFolderName = constants.getIosConfigurationFolderName();
    const animationsRelativeGroupPath = path.join(configFolderName, constants.getAnimationsConfigurationFolderName());
    const imagesRelativeGroupPath = path.join(configFolderName, constants.getImagesConfigurationFolderName());
    const fontsRelativeGroupPath = path.join(configFolderName, constants.getFontsConfigurationFolderName());
    const projectPath = iosUtilities.getPbxprojPath(context);
    const project = xcode.project(projectPath);
    
    project.parseSync();

    //Add mandatory folders
    addResourceFilesFromDir(configurationPlatformPath, configFolderName, project);
    addResourceFilesFromDir(platformAnimationsPath, animationsRelativeGroupPath,  project);
    addResourceFilesFromDir(platformFontsPath, fontsRelativeGroupPath, project);

    //Add optional folders
    if (utilities.existsDir(platformImagesPath)) {
        logger.log('Adding images files');
        addResourceFilesFromDir(platformImagesPath, imagesRelativeGroupPath,  project);   
    }
    
    fs.writeFileSync(projectPath, project.writeSync());
}

module.exports = function(context) {
    logger.log('Adding Assistant configurations');

    const configurationPlatformPath = path.join(iosUtilities.getResourcesPath(context), constants.getIosConfigurationFolderName());
    configurationManager.copyConfiguration(context, configurationPlatformPath);

    copyConfigurationToProject(context, configurationPlatformPath);
}