const logger = require('../logger');
const utilities = require('../utilities');
const podUtilities = require('./podUtilities');

function removePodFromPodfile(context, podfile) {
  const podfileLines = podfile.split('\n');
  var newPodfileLines = [];

  podfileLines.forEach( line => {
    const podRegex = new RegExp('pod \'AgentifaiAssistant\',(.*)');

    if (!podRegex.exec(line)) {
      newPodfileLines.push(line);
    }
  })

  const newPodfile = newPodfileLines.join('\n');
  podUtilities.writePodfile(context, newPodfile);
}

module.exports = function(context) {
  const podfilePath = podUtilities.getPodfilePath(context);

  if (utilities.existsFile(podfilePath)) {
    logger.log('Removing Agentifai pod from podfile');
    const podfile = podUtilities.getPodfile(context);
    removePodFromPodfile(context, podfile);
  }
}