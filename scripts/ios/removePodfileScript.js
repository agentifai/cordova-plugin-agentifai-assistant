const path = require('path');
const logger = require('../logger');
const utilities = require('../utilities');
const podUtilities = require('./podUtilities');

module.exports = function(context) {
  const podfilePath = podUtilities.getPodfilePath(context);

  if (utilities.existsFile(podfilePath)) {
    logger.log('Removing post install script from podfile');
    const scriptPath = path.join(context.opts.plugin.dir, 'scripts/ios/PostInstallScript.txt');
    const script = utilities.readFile(scriptPath);
    const fullScript = '#AgentifaiAssistant post install script\n' + script;
    const data = podUtilities.getPodfile(context)

    if (data.includes(fullScript)) {
      const newData = data.replace(fullScript, '');
      utilities.writeFile(podfilePath, newData);
    }
  }
}