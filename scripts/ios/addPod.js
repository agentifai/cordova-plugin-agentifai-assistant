const logger = require('../logger');
const utilities = require('../utilities');
const podUtilities = require('./podUtilities');

// Must be the same defined in plugin.xml
const IOS_URL_KEY = 'AGENTIFAI_ASSISTANT_IOS_URL';
const IOS_VERSION_KEY = 'AGENTIFAI_ASSISTANT_IOS_VERSION';

function isValid(value) {
  return value !== null && value !== undefined && value !== '';
}

function addPodToPodfile(context, podUrl, podVersion) {
  const podfile = podUtilities.getPodfile(context);
  const podfileLines = podfile.split('\n');
  var newPodfileLines = [];

  podfileLines.forEach( (line, index) => {
    newPodfileLines.push(line)

    const targetRegex = new RegExp('target\\s+\'[^\']+\'\\s+do');
    const projectRegex = new RegExp('project\\s+\'[^\']+\'');
    if (
      projectRegex.test(line) 
      && index > 0
      && targetRegex.test(podfileLines[index - 1])
    ) {
      newPodfileLines.push(`\tpod 'AgentifaiAssistant', :git => '${podUrl}', :tag => '${podVersion}'`);
    }
  })

  const newPodfile = newPodfileLines.join('\n');
  podUtilities.writePodfile(context, newPodfile);
}

module.exports = function(context) {
  logger.log('Adding Agentifai pod to podfile');

  const configFile = utilities.getConfigFile();
  const pluginFile = utilities.getPluginFile(context);

  //Get values from config.xml
  var podUrl = utilities.getPreferenceValue(configFile, IOS_URL_KEY);
  var podVersion = utilities.getPreferenceValue(configFile, IOS_VERSION_KEY);

  //Get variables from plugin installation command
  if (!isValid(podUrl)) {
    podUrl = utilities.getVariableValue(process.argv, IOS_URL_KEY);
  }

  if (!isValid(podVersion)) {
    podVersion = utilities.getVariableValue(process.argv, IOS_VERSION_KEY);
  }

  //Get default values from plugin.xml
  if (!isValid(podVersion)) {
    podVersion = utilities.getPreferenceValue(pluginFile, IOS_VERSION_KEY);
  }

  if (!isValid(podUrl)) {
    throw new Error(`${IOS_URL_KEY} not found. Add ${IOS_URL_KEY} as plugin variable`);
  }

  if (!isValid(podVersion)) {
    throw new Error(`${IOS_VERSION_KEY} not found. Add ${IOS_VERSION_KEY} as plugin variable`);
  }

  logger.log(`iOS assistant URL: ${podUrl}`);
  logger.log(`iOS assistant version: ${podVersion}`);

  addPodToPodfile(context, podUrl, podVersion);
}