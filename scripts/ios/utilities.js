const path = require('path');
const utilities = require('../utilities');

module.exports = {
  /**
   * Get iOS platform path
   */
  getPlatformPath: function(context) {
    return path.join(utilities.getRootPath(context),'platforms/ios');
  },

  /**
   * Get iOS Resources path
   */
  getResourcesPath: function(context) {
    const platformPath = this.getPlatformPath(context);
    const appName = utilities.getAppName(context);

    return path.join(platformPath, appName, 'Resources');
  },

  /**
   * Get pbxproj path
   */
  getPbxprojPath: function(context) {
    return path.join(this.getPlatformPath(context), `${utilities.getAppName(context)}.xcodeproj/project.pbxproj`);
  }

}
