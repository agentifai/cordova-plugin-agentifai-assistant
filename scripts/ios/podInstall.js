const podUtilities = require('./podUtilities');

/**
 * Force pod install if podfile has not been changed by cordova
 */
module.exports = function(context) {
  podUtilities.executePodInstall(context);
}