const logger = require("../logger");
const path = require('path');
const utilities = require('../utilities');
const iosUtilities = require('./utilities');
const constants = require('../constants');
const fs = require('fs');
const xcode = require('xcode')

function getFiles(filespath) {
    return fs.readdirSync(filespath);
}

function removeResourceFilesFromDir(dir, project) {
    if (utilities.existsDir(dir)) {
        const files = getFiles(dir)
        files.forEach(file => {
            project.removeResourceFile(path.join(dir, file));
        })
    }
}

module.exports = function(context) {
    logger.log('Removing Assistant configurations');

    const configurationPlatformPath = path.join(iosUtilities.getResourcesPath(context), constants.getIosConfigurationFolderName());
    const platformAnimationsPath = path.join(configurationPlatformPath, constants.getAnimationsConfigurationFolderName());
    const platformImagesPath = path.join(configurationPlatformPath, constants.getImagesConfigurationFolderName());
    const platformFontsPath = path.join(configurationPlatformPath, constants.getFontsConfigurationFolderName());
    
    const projectPath = iosUtilities.getPbxprojPath(context);
    const project = xcode.project(projectPath);
    
    project.parseSync();
    
    removeResourceFilesFromDir(configurationPlatformPath, project);
    removeResourceFilesFromDir(platformAnimationsPath, project);
    removeResourceFilesFromDir(platformImagesPath, project);
    removeResourceFilesFromDir(platformFontsPath, project);

    fs.writeFileSync(projectPath, project.writeSync());
    utilities.removeDir(configurationPlatformPath);
}