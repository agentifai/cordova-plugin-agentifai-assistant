const fs = require('fs');
const path = require('path');
const logger = require('../logger');
const utilities = require('../utilities');
const podUtilities = require('./podUtilities');

function isScriptAdded(podfilePath, script) {
    const data = utilities.readFile(podfilePath);
    return data.includes(script);
}

function writeScript(podfilePath, script) {
    const fullScript = '#AgentifaiAssistant post install script\n' + script;
    utilities.appendFile(podfilePath, fullScript);
}

module.exports = function(context) {
    logger.log('Adding post install script to podfile');
    const podfilePath = podUtilities.getPodfilePath(context);
    const scriptPath = path.join(context.opts.plugin.dir, "scripts/ios/PostInstallScript.txt");
    const script = fs.readFileSync(scriptPath, 'utf8')

    if (!isScriptAdded(podfilePath, script)) {
        writeScript(podfilePath, script);
    }
}