const { spawnSync } = require('child_process');
const path = require('path');
const logger = require('../logger');
const utilities = require('../utilities');

module.exports = {
  /**
    * Used to get path of podfile
  */
  getPodfilePath: function(context) {
    return path.join(context.opts.projectRoot, 'platforms/ios/Podfile');
  },
  
  /**
    * Used to get podfile
  */
  getPodfile: function(context) {
    const podfilePath = this.getPodfilePath(context);
    return utilities.readFile(podfilePath);
  },

  /**
    * Used to write to podfile
  */
  writePodfile: function(context, data) {
    const file = this.getPodfilePath(context);
    utilities.writeFile(file, data);
  },

  getPodfileLockFile: function(context) {
    const rootPath = context.opts.projectRoot;
    const podfileLockPath = path.join(rootPath, 'platforms', 'ios', 'Podfile.lock');

    if (!utilities.existsFile(podfileLockPath)) {
      return null;
    }

    return utilities.readFile(podfileLockPath).toString();
  },

  /**
   * Check if the AgentifaiAssistant pod exists in the dependency section of the Podfile.lock file
   */
  isPodInstalled: function(context) {
    const podFileLockFile = this.getPodfileLockFile(context);
    if (!podFileLockFile) { return false; }

    const podDependencyRegex = new RegExp('DEPENDENCIES:[\\s\\\S]*AgentifaiAssistant.*');
    return podDependencyRegex.test(podFileLockFile);
  },

  executePodInstall: function(context) {
    const rootPath = context.opts.projectRoot;
    const opts = {};
    opts.cwd = path.join(rootPath, 'platforms', 'ios');

    logger.log("Agentifai executing pod install");
    logger.log(utilities.readFile(this.getPodfilePath(context)))

    var command = spawnSync('pod', ['install', '--verbose'], opts);
    logger.log(String(command.stdout));
  }

}