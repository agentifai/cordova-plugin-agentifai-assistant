
var PLUGIN_NAME = "Agentifai";
var agentifai = {
    
    initialize: function (apiUrl, apiKey, settings, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'initialize', [apiUrl, apiKey, settings]);
    },

    setConfigurations: function(configurations, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'setConfigurations', [configurations]);
    },

    openApplication: function (intent, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'openApplication', [intent]);
    },
    
    closeApplication: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'closeApplication', []);
    },
                   
    stop: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'stop', []);
    },
    
    requestResult: function (id, successResponse, data, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'requestResult', [id, successResponse, data]);
    },

    registerUser: function (user, authHeaders, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'registerUser', [user, authHeaders]);
    },

    unregisterUser: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'unregisterUser', []);
    },
    
    addClientCallback: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'addClientCallback', []);
    },

    setTheme: function(theme, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'setTheme', [theme]);
    },

    //Authorization
    enableAuthorization: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'enableAuthorization', []);
    },

    requestAuthorizationAvailability: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'requestAuthorizationAvailability', []);
    },

    authorizationAvailabilityResult: function (result, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'authorizationAvailabilityResult', [result]);
    },
    
    requestAuthorization: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'requestAuthorization', []);
    },

    authorizationResult: function (result, success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'authorizationResult', [result]);
    },

    //Notifications
    enableNotifications: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'enableNotifications', []);
    },

    requestNotification: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'requestNotification', []);
    },

    requestTotalNotificationsUnread: function (success, error) {
        cordova.exec(success, error, PLUGIN_NAME, 'requestTotalNotificationsUnread', []);
    }

}

module.exports = agentifai;