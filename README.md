# Agentifai Assistant Plugin
This plugin provides an Agentifai Assistant SDK for mobile apps to communicate with an artificial intelligence assistant.

## Installation

### Cordova
To install the plugin in your Cordova app, run the following:
```
cordova plugin add https://bitbucket.org/agentifai/cordova-plugin-agentifai-assistant.git
```

### Configuration
You need to configure access to the get iOS and Android frameworks from private repositories.
Then set your Agentifai repositories URL's into your config.xml (Android / iOS).

```xml
<preference name="AGENTIFAI_ASSISTANT_ANDROID_URL" value="(maven repository url)" />
<preference name="AGENTIFAI_ASSISTANT_IOS_URL" value="(pod repository url)" />
```

#### Repositories Authentication
The native frameworks are present in private repositories and you need configure the access to these repositories.

The iOS repository authentication is done when the Agentifai Assistant pod is installed, but if you prefer, you can create an ssh key to grant access to your repository without having to enter your credentials at any time.

Android repository authentication is done adding the credentials in your local gradle properties.
In gradle.properties located in your ```GRADLE_USER_HOME``` directory, which is usually ```~/.gradle/gradle.properties``` add this code with your Bitbucket account:
```gradle
AGENTIFAI_ASSISTANT_USERNAME=#username
AGENTIFAI_ASSISTANT_PASSWORD=#password
```
You can also add these properties to your config.xml, but we discourage you because you are exposing your credentials in your project.

#### Install optional variables (config.xml)
You can configure version of native assistant framework by each platform(Android / iOS).
By default the latest version is installed.
To get available versions you can access in native frameworks repositories and see information about each version.

```xml
<preference name="AGENTIFAI_ASSISTANT_ANDROID_VERSION" value="(android version)" />
<preference name="AGENTIFAI_ASSISTANT_IOS_VERSION" value="(ios version)" />
```