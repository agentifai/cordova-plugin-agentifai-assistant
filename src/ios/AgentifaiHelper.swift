typealias JSON = [String: Any]

class AgentifaiHelper {
    
    static func decode<T: Decodable>(from json: JSON) throws -> T {
        let data = try self.serialize(json: json)
        return try JSONDecoder().decode(T.self, from: data)
    }
    
    static func encode<T: Encodable>(from object: T) throws -> JSON {
        let jsonEncoder = JSONEncoder()
        let jsonData = try jsonEncoder.encode(object)
        return try JSONSerialization.jsonObject(
            with: jsonData,
            options: JSONSerialization.ReadingOptions.allowFragments
        ) as! JSON
    }
    
    static func getErrorDescription(error: Error) -> String {
        if let pluginError = error as? AgentifaiPluginError {
            return pluginError.message
        } else {
            return "[AgentifaiBridge] " + error.localizedDescription
        }
    }
    
    private static func serialize(json: JSON) throws -> Data {
        return try JSONSerialization.data(withJSONObject: json, options: .fragmentsAllowed)
    }
    
}
