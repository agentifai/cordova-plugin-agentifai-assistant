import AgentifaiAssistant

@objc(AgentifaiBridge)
class AgentifaiBridge : CDVPlugin {
    
    private var clientCommand: CDVInvokedUrlCommand?
    private var authorizationAvailabilityCommand: CDVInvokedUrlCommand?
    private var authorizationCommand: CDVInvokedUrlCommand?
    private var notificationCommand: CDVInvokedUrlCommand?
    private var totalNotificationsUnreadCommand: CDVInvokedUrlCommand?

    @objc(initialize:) func initialize(command: CDVInvokedUrlCommand) {
        guard let apiUrl = command.argument(at: 0) as? String,
            let apiKey = command.argument(at: 1) as? String,
            let settings = command.argument(at: 2) as? JSON
            else {
                self.sendError(command: command)
                return
        }
        
        Agentifai.initialize(apiUrl: apiUrl, apiKey: apiKey, settings: settings)
        Agentifai.setDelegate(delegate: self)

        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
    }

    @objc(setConfigurations:) func setConfigurations(command: CDVInvokedUrlCommand) {
        guard let data = command.argument(at: 0) as? JSON else {
            self.sendError(command: command)
            return
        }
        
        do {
            let configurations: ClientApiConfigurations = try AgentifaiHelper.decode(from: data)
            Agentifai.setConfigurations(configurations: configurations)
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
        } catch {
           self.sendError(command: command)
        }
    }

    @objc(openApplication:) func openApplication(command: CDVInvokedUrlCommand) {
        let intent = command.arguments.first as? String
        Agentifai.openApplication(intent: intent)
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
    }

    @objc(closeApplication:) func closeApplication(command: CDVInvokedUrlCommand) {
        Agentifai.closeApplication()
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
    }
    
    @objc(stop:) func stop(command: CDVInvokedUrlCommand) {
        Agentifai.stop()
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
    }
    
    @objc(registerUser:) func registerUser(command: CDVInvokedUrlCommand) {
        guard let userJson = command.argument(at: 0) as? JSON,
            let id = userJson["id"] as? String,
            let authHeaders = command.argument(at: 1) as? [String: String]
        else {
            self.sendError(command: command)
            return
        }

        let name = userJson["name"] as? String
        let token = userJson["token"] as? String

        self.commandDelegate?.run(inBackground: {
            let user = AgentifaiUser(id: id, name: name, token: token)
            Agentifai.register(user: user, authHeaders: authHeaders)
        
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
        })
    }

    @objc(unregisterUser:) func unregisterUser(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.run(inBackground: {
            Agentifai.unregisterUser()
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
        })
    }
    
    @objc(requestResult:) func requestResult(command: CDVInvokedUrlCommand) {
        guard let requestId = command.argument(at: 0) as? String,
            let success = command.argument(at: 1) as? Bool,
            var response = command.argument(at: 2) as? JSON
            else {
                self.sendError(command: command)
                return
        }
        
        do {
            response["requestId"] = requestId
            response["success"] = success
            let hostResponse: HostAppResponse = try AgentifaiHelper.decode(from: response)
            
            self.commandDelegate?.run(inBackground: {
                Agentifai.requestResult(response: hostResponse)
                let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
                self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
            })
        }
        catch {
            self.sendError(command: command)
            self.sendErrorResponse(requestId: requestId, error: error)
        }
    }
    
    @objc(addClientCallback:) func addClientCallback(command: CDVInvokedUrlCommand) {
        self.clientCommand = command
    }

    @objc(setTheme:) func setTheme(command: CDVInvokedUrlCommand) {
        guard let theme = command.arguments.first as? String else {
                self.sendError(command: command)
                return
        }
        
        Agentifai.setTheme(theme: theme)
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
    }
    
    //Authorization
    @objc(enableAuthorization:) func enableAuthorization(command: CDVInvokedUrlCommand) {
        Agentifai.setAuthorizationDelegate(delegate: self)
        self.commandDelegate?.send(CDVPluginResult(status: CDVCommandStatus_OK), callbackId: command.callbackId)
    }

    @objc(requestAuthorizationAvailability:) func requestAuthorizationAvailability(command: CDVInvokedUrlCommand) {
        self.authorizationAvailabilityCommand = command
    }

    @objc(authorizationAvailabilityResult:) func authorizationAvailabilityResult(command: CDVInvokedUrlCommand) {
        guard let data = command.argument(at: 0) as? JSON else {
                self.sendError(command: command)
                return
        }
        
        do {
            let response: AuthorizationAvailabilityResponse = try AgentifaiHelper.decode(from: data)
            Agentifai.authorizationAvailabilityResult(response: response)
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
        } catch {
           self.sendError(command: command)
        }
    }

    @objc(requestAuthorization:) func requestAuthorization(command: CDVInvokedUrlCommand) {
        self.authorizationCommand = command
    }

    @objc(authorizationResult:) func authorizationResult(command: CDVInvokedUrlCommand) {
        guard let data = command.argument(at: 0) as? JSON else {
                self.sendError(command: command)
                return
        }
        
        do {
            let anyResponse: AnyAuthorizationResponse = try AgentifaiHelper.decode(from: data)
            Agentifai.authorizationResult(response: anyResponse.response)
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            self.commandDelegate?.send(pluginResult, callbackId: command.callbackId)
        } catch {
           self.sendError(command: command)
        }
    }

    //Notifications
    @objc(enableNotifications:) func enableNotifications(command: CDVInvokedUrlCommand) {
        Agentifai.setNotificationsDelegate(delegate: self)
        self.commandDelegate?.send(CDVPluginResult(status: CDVCommandStatus_OK), callbackId: command.callbackId)
    }

    @objc(requestNotification:) func requestNotification(command: CDVInvokedUrlCommand) {
        notificationCommand = command
    }

    @objc(requestTotalNotificationsUnread:) func requestTotalNotificationsUnread(command: CDVInvokedUrlCommand) {
        totalNotificationsUnreadCommand = command
    }

    private func sendResult(data: JSON, command: CDVInvokedUrlCommand?) throws {
        guard let delegate = self.commandDelegate else {
            throw AgentifaiPluginError.commandDelegateNotExists
        }
        
        guard let callback = command else {
            throw AgentifaiPluginError.callbackNotExists
        }
        
        delegate.run(inBackground: {
            let pluginResult: CDVPluginResult! = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: data
            )
            pluginResult.keepCallback = true
        
            delegate.send(pluginResult, callbackId: callback.callbackId)
        })
    }
    
    private func sendError(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.run(inBackground: {
            let result = CDVPluginResult(status: CDVCommandStatus_ERROR)
            self.commandDelegate.send(result, callbackId: command.callbackId)
        })
    }
    
    private func sendErrorResponse(requestId: String, error: Error) {
        let nonAnonymizableRegexPaths = ["*"]
        let data: [String : Any] = [
            "status": 550,
            "body": [
                "error": AgentifaiHelper.getErrorDescription(error: error)
            ]
        ]

        let response = HostAppResponse(requestId: requestId, success: false, data: data, nonAnonymizableRegexPaths: nonAnonymizableRegexPaths)
        Agentifai.requestResult(response: response)
    }
    
}

extension AgentifaiBridge: AgentifaiProtocol {
    
    func request(id: String, data: JSON) {
        var newData: JSON = data
        newData["type"] = "request"
        newData["requestId"] = id
        
        do {
            try self.sendResult(data: newData, command: clientCommand)
        }
        catch {
            self.sendErrorResponse(requestId: id, error: error)
        }
    }
    
    func agentifaiStatus(isOpened: Bool) {
        var newData: JSON = [:]
        newData["type"] = "applicationStatus"
        newData["isOpened"] = isOpened
        
        do {
            try self.sendResult(data: newData, command: clientCommand)
        }
        catch {}
    }

    func receiveData(data: [String: Any]) {
        var newData: JSON = [:]
        newData["type"] = "receiveData"
        newData["data"] = data

        do {
            try self.sendResult(data: newData, command: clientCommand)
        }
        catch {}
    }
    
}

extension AgentifaiBridge: AuthorizationDelegate {
    
    func requestAuthorizationAvailability(request: AuthorizationAvailabilityRequest) {
        do {
            let data = try AgentifaiHelper.encode(from: request)
            try sendResult(data: data, command: authorizationAvailabilityCommand)
        }
        catch {
            Agentifai.authorizationAvailabilityResult(response: AuthorizationAvailabilityResponse(requestId: request.id, available: false))
        }
    }
    
    func requestAuthorization(request: AgentifaiAssistant.AuthorizationRequest) {
        do {
            let data = try AgentifaiHelper.encode(from: request)
            try sendResult(data: data, command: authorizationCommand)
        }
        catch {
            Agentifai.authorizationResult(response: AuthorizationResponseFailure(requestId: request.id))
        }
    }

}

extension AgentifaiBridge: NotificationsDelegate {

    func receiveNotification(notification: String) {
        do {
            let data = ["notification": notification]
            try sendResult(data: data, command: notificationCommand)
        } catch {}
    }

    func receiveTotalNotificationsUnread(total: Int) {
        do {
            let data = ["total": total]
            try sendResult(data: data, command: totalNotificationsUnreadCommand)
        } catch {}
    }
}
