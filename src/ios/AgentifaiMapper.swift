
import AgentifaiAssistant

class AnyAuthorizationResponse: Decodable {
    let response: AuthorizationResponse
    
    init(response: AuthorizationResponse) {
        self.response = response
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let success = try? container.decode(AuthorizationResponseSuccess.self) {
            response = success
        } else if let failure = try? container.decode(AuthorizationResponseFailure.self) {
            response = failure
        } else {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid response type")
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch response {
        case let success as AuthorizationResponseSuccess:
            try container.encode(success)
        case let failure as AuthorizationResponseFailure:
            try container.encode(failure)
        default:
            throw EncodingError.invalidValue(
                response,
                EncodingError.Context(codingPath: container.codingPath, debugDescription: "Invalid response type")
            )
        }
    }
}