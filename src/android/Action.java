package cordova.plugin.agentifai;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.agentifai.assistant.configurations.clientApi.ApiEndpointConfiguration;
import com.agentifai.assistant.configurations.clientApi.ClientApiConfiguration;
import com.agentifai.assistant.models.Agentifai;
import com.agentifai.assistant.models.authorization.AuthorizationAvailabilityResponse;
import com.agentifai.assistant.models.authorization.AuthorizationResponse;
import com.agentifai.assistant.models.data.commands.httpRequestCommand.HttpRequestMethod;
import com.agentifai.assistant.models.data.events.models.HostAppResponse;
import com.agentifai.assistant.models.data.user.AgentifaiUser;

enum Action {

    initialize {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            String apiUrl = args.optString(0);
            String apiKey = args.optString(1);
            JSONObject settings = args.optJSONObject(2);

            if (!args.isNull(0) && !args.isNull(1) && !apiUrl.isEmpty() && !apiKey.isEmpty() && settings != null) {
                Map<String, Object> clientSettings = AgentifaiHelper.mapFromJSON(settings);
                Agentifai.INSTANCE.initialize(cordova.getActivity().getApplication(), apiUrl, apiKey, clientSettings);
                Agentifai.INSTANCE.setListener(listener);
                callbackContext.success();
            } else {
                callbackContext.error("Arguments to initialize are empty");
            }
        }
    },

    setConfigurations {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            JSONObject configurations = args.optJSONObject(0);
            ClientApiConfiguration clientApiConfiguration = null;

            try {
                String url = configurations.getString("url");
                Map<String, String> headers = AgentifaiHelper.mapStringStringFromJSON(configurations.getJSONObject("headers"));
                Map<String, String> headersKeysMapping = AgentifaiHelper.mapStringStringFromJSON(configurations.getJSONObject("headersKeysMapping"));
                JSONArray endpointsJson = configurations.optJSONArray("endpoints");
                List<ApiEndpointConfiguration> endpoints = new ArrayList<>();
                if (endpointsJson != null) {
                    for (int i = 0; i < endpointsJson.length(); i++) {
                        JSONObject jsonObject = endpointsJson.getJSONObject(i);
                        String endpoint = jsonObject.getString("endpoint");
                        List<String> successWhitelist = AgentifaiHelper.listFromJson(jsonObject.getJSONArray("successWhitelist"));
                        List<String> failureWhitelist = AgentifaiHelper.listFromJson(jsonObject.getJSONArray("failureWhitelist"));
                        String httpMethod = jsonObject.getString("httpMethod");
                        HttpRequestMethod httpRequestMethod = HttpRequestMethod.Companion.from(httpMethod);
                        endpoints.add(new ApiEndpointConfiguration(endpoint, httpRequestMethod, successWhitelist, failureWhitelist));
                    }
                }
                clientApiConfiguration = new ClientApiConfiguration(
                        url,
                        headers,
                        headersKeysMapping,
                        endpoints
                );
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (clientApiConfiguration != null) {
                Agentifai.INSTANCE.setConfigurations(clientApiConfiguration);
                callbackContext.success();
            } else {
                callbackContext.error("Expected one non-empty argument to setConfigurations");
            }
        }
    },

    openApplication {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            String intent = null;
            if (!args.isNull(0)) {
                intent = args.optString(0);
            }
            Agentifai.INSTANCE.openApplication(intent);
            callbackContext.success();
        }
    },

    closeApplication {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            Agentifai.INSTANCE.closeApplication();
        }
    },

    stop {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            Agentifai.INSTANCE.stop();
            callbackContext.success();
        }
    },

    registerUser {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            JSONObject userJson = args.optJSONObject(0);
            String id = userJson.optString("id");
            String name = AgentifaiHelper.optString(userJson, "name");
            String token = AgentifaiHelper.optString(userJson, "token");

            JSONObject authHeadersJson = args.optJSONObject(1);

            if (!id.isEmpty() && authHeadersJson != null) {
                Map<String, String> authHeaders = AgentifaiHelper.mapStringStringFromJSON(authHeadersJson);
                AgentifaiUser user = new AgentifaiUser(id, name, token);

                Agentifai.INSTANCE.registerUser(user, authHeaders);
                callbackContext.success();
            } else {
                callbackContext.error("Arguments to registerUser are empty");
            }
        }
    },

    unregisterUser {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            Agentifai.INSTANCE.unregisterUser();
            callbackContext.success();
        }
    },

    requestResult {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            String requestId = args.optString(0);
            Boolean success = args.optBoolean(1);
            JSONObject clientResponse = args.optJSONObject(2);

            try {
                JSONObject responseData = clientResponse.getJSONObject("data");
                JSONArray responseNonAnonymizableRegexPaths = clientResponse.getJSONArray("nonAnonymizableRegexPaths");
                Map<String, Object> data = AgentifaiHelper.mapFromJSON(responseData);
                List<String> nonAnonymizableRegexPaths = AgentifaiHelper.listFromJson(responseNonAnonymizableRegexPaths);

                listener.sendRequestResult(new HostAppResponse(requestId, success, data, nonAnonymizableRegexPaths));
                callbackContext.success();

            } catch (JSONException e) {
                listener.sendErrorResponse(requestId, e);
            }
        }
    },

    addClientCallback {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            listener.addClientCallback(callbackContext);
        }
    },

    setTheme {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            String theme = args.optString(0);

            if (!args.isNull(0) && !theme.isEmpty()) {
                Agentifai.INSTANCE.setTheme(theme);
                callbackContext.success();
            } else {
                callbackContext.error("Expected one non-empty string argument.");
            }
        }
    },

    enableAuthorization {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            Agentifai.INSTANCE.setAuthorizationListener(listener);
            callbackContext.success();
        }
    },

    requestAuthorizationAvailability {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            listener.requestAuthorizationAvailability(callbackContext);
        }
    },

    authorizationAvailabilityResult {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            try {
                JSONObject json = args.getJSONObject(0);
                AuthorizationAvailabilityResponse response = new AuthorizationAvailabilityResponse(
                        json.getString("requestId"),
                        json.getBoolean("available")
                );

                listener.authorizationAvailabilityResult(response);
                callbackContext.success();

            } catch (JSONException e) {
                callbackContext.error(e.getMessage());
            }
        }
    },

    requestAuthorization {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            listener.requestAuthorization(callbackContext);
        }
    },

    authorizationResult {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            try {
                JSONObject json = args.getJSONObject(0);
                String requestId = json.getString("requestId");
                String authorization = json.optString("authorization");
                AuthorizationResponse response;

                if (!authorization.isEmpty()) {
                    response = new AuthorizationResponse.Success(
                            requestId,
                            authorization
                    );
                } else {
                    response = new AuthorizationResponse.Failure(requestId);
                }

                listener.authorizationResult(response);
                callbackContext.success();
            } catch (JSONException e) {
                callbackContext.error(e.getMessage());
            }
        }
    },

    enableNotifications {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            Agentifai.INSTANCE.setNotificationsListener(listener);
            callbackContext.success();
        }
    },

    requestNotification {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            listener.requestNotification(callbackContext);
        }
    },

    requestTotalNotificationsUnread {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            listener.requestTotalNotificationsUnread(callbackContext);
        }
    },

    unknown {
        @Override
        void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener) {
            callbackContext.error("Non action exists");
        }
    };

    abstract void execute(CordovaInterface cordova, JSONArray args, CallbackContext callbackContext, ActionsListener listener);

    public static Action fromString(String value) {
        Action action = unknown;

        try {
            action = Action.valueOf(value);
        } catch (Exception e) {
        }

        return action;
    }

}