package cordova.plugin.agentifai;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class AgentifaiHelper {

    public static Map<String, Object> mapFromJSON(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keysIter = jsonObject.keys();
        while (keysIter.hasNext()) {
            String key = keysIter.next();
            if (!jsonObject.isNull(key)) {
                Object value = getObject(jsonObject.opt(key));
                map.put(key, value);
            }
        }
        return map;
    }

    public static Map<String, String> mapStringStringFromJSON(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        Map<String, String> map = new HashMap<String, String>();
        Iterator<String> keysIter = jsonObject.keys();
        while (keysIter.hasNext()) {
            String key = keysIter.next();
            String value = jsonObject.optString(key);
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> listFromJSON(JSONArray jsonArray) {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0, count = jsonArray.length(); i < count; i++) {
            Object value = getObject(jsonArray.opt(i));
            if (value != null) {
                list.add(value);
            }
        }
        return list;
    }

    public static List<String> listFromJson(JSONArray jsonArray) {
        List<String> list = new ArrayList();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.optString(i));
        }
        return list;
    }

    public static Object getObject(Object value) {
        if (value instanceof JSONObject) {
            value = mapFromJSON((JSONObject) value);
        } else if (value instanceof JSONArray) {
            value = listFromJSON((JSONArray) value);
        }
        return value;
    }

    public static List<String> getStackTrace(Throwable throwable, int max) {
        List<String> stackTrace = new ArrayList<>();

        for (int i = 0; i < throwable.getStackTrace().length && i < max ; i++) {
            if (throwable.getStackTrace()[i] != null) {
                stackTrace.add(throwable.getStackTrace()[i].toString());
            }
        }

        return stackTrace;
    }

    /** Return the value mapped by the given key, or {@code null} if not present or null. */
    public static String optString(JSONObject json, String key)
    {
        if (json.isNull(key))
            return null;
        else
            return json.optString(key);
    }

} 