package cordova.plugin.agentifai;

import android.content.ActivityNotFoundException;
import android.util.NoSuchPropertyException;

import androidx.annotation.NonNull;

import com.agentifai.assistant.models.Agentifai;
import com.agentifai.assistant.models.authorization.AuthorizationAvailabilityRequest;
import com.agentifai.assistant.models.authorization.AuthorizationAvailabilityResponse;
import com.agentifai.assistant.models.authorization.AuthorizationRequest;
import com.agentifai.assistant.models.authorization.AuthorizationResponse;
import com.agentifai.assistant.models.data.events.models.HostAppResponse;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AgentifaiBridge extends CordovaPlugin implements ActionsListener {

    private CallbackContext clientCallback;
    private CallbackContext authorizationAvailabilityCallback;
    private CallbackContext authorizationCallback;
    private CallbackContext notificationCallback;
    private CallbackContext totalNotificationsUnreadCallback;

    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) {
        final Action act = Action.fromString(action);
        this.execute(act, this.cordova, args, callbackContext, this);
        return act != Action.unknown;
    }

    /**
     * Actions Listener
     */
    @Override
    public void addClientCallback(CallbackContext callbackContext) {
        this.clientCallback = callbackContext;
    }

    @Override
    public void sendRequestResult(HostAppResponse response) {
        Agentifai.INSTANCE.requestResult(response);
    }

    @Override
    public void sendErrorResponse(String requestId, Throwable error) {
        List<String> nonAnonymizableRegexPaths = new ArrayList<>();
        nonAnonymizableRegexPaths.add("*");

        Map<String, Object> body = new HashMap<>();
        body.put("error", "[AgentifaiBridge] " + error.toString());
        body.put("stackTrace", AgentifaiHelper.getStackTrace(error, 10));

        Map<String, Object> data = new HashMap<>();
        data.put("body", body);
        data.put("status", 550);

        HostAppResponse response = new HostAppResponse(requestId, false, data, nonAnonymizableRegexPaths);
        this.sendRequestResult(response);
    }

    /**
     * Agentifai Listener
     */
    @Override
    public void request(String requestId, Map<String, ?> map) {
        try {
            JSONObject data = new JSONObject(map);
            data.put("type", "request");
            data.put("requestId", requestId);
            this.sendResult(data, clientCallback);
        } catch (Exception e) {
            this.sendErrorResponse(requestId, e);
        }
    }

    @Override
    public void agentifaiStatus(boolean isOpened) {
        try {
            JSONObject data = new JSONObject();
            data.put("type", "applicationStatus");
            data.put("isOpened", isOpened);

            this.sendResult(data, clientCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveData(Map<String, ?> data) {
        try {
            JSONObject newData = new JSONObject();
            newData.put("type", "receiveData");
            newData.put("data", new JSONObject(data));

            this.sendResult(newData, clientCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Authorization Listener
     */
    @Override
    public void requestAuthorizationAvailability(CallbackContext callbackContext) {
        this.authorizationAvailabilityCallback = callbackContext;
    }

    @Override
    public void requestAuthorizationAvailability(@NonNull AuthorizationAvailabilityRequest request) {
        try {
            JSONObject data = new JSONObject();
            data.put("id", request.getId());

            sendResult(data, authorizationAvailabilityCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void authorizationAvailabilityResult(AuthorizationAvailabilityResponse response) {
        Agentifai.INSTANCE.authorizationAvailabilityResult(response);
    }

    @Override
    public void requestAuthorization(CallbackContext callbackContext) {
        this.authorizationCallback = callbackContext;
    }

    @Override
    public void requestAuthorization(@NonNull AuthorizationRequest request) {
        try {
            JSONObject data = new JSONObject();
            data.put("id", request.getId());

            sendResult(data, authorizationCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void authorizationResult(AuthorizationResponse response) {
        Agentifai.INSTANCE.authorizationResult(response);
    }

    @Override
    public void requestNotification(CallbackContext callbackContext) {
        notificationCallback = callbackContext;
    }

    @Override
    public void requestTotalNotificationsUnread(CallbackContext callbackContext) {
        totalNotificationsUnreadCallback = callbackContext;
    }

    @Override
    public void receiveNotification(@NonNull String notification) {
        try {
            JSONObject data = new JSONObject();
            data.put("notification", notification);

            sendResult(data, notificationCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveTotalNotificationsUnread(int total) {
        try {
            JSONObject data = new JSONObject();
            data.put("total", total);

            sendResult(data, totalNotificationsUnreadCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void execute(
            final Action action,
            final CordovaInterface cordova,
            final JSONArray args,
            final CallbackContext callbackContext,
            final ActionsListener listener
    ) {
        cordova.getThreadPool().execute(() -> action.execute(cordova, args, callbackContext, listener));
    }

    private void sendResult(JSONObject data, CallbackContext callbackContext) {
        if (this.cordova.getActivity().isDestroyed()) {
            throw new ActivityNotFoundException("Cordova activity has been destroyed");
        }
        if (callbackContext == null) {
            throw new NoSuchPropertyException("The plugin callback does not exist");
        }

        PluginResult result = new PluginResult(PluginResult.Status.OK, data);
        result.setKeepCallback(true);

        callbackContext.sendPluginResult(result);
    }

}