package cordova.plugin.agentifai;

import org.apache.cordova.CallbackContext;
import com.agentifai.assistant.models.AgentifaiListener;
import com.agentifai.assistant.models.AuthorizationListener;
import com.agentifai.assistant.models.authorization.AuthorizationAvailabilityResponse;
import com.agentifai.assistant.models.authorization.AuthorizationResponse;
import com.agentifai.assistant.models.data.events.models.HostAppResponse;
import com.agentifai.assistant.models.notification.NotificationsListener;

interface ActionsListener extends AgentifaiListener, AuthorizationListener, NotificationsListener {
    void addClientCallback(CallbackContext callbackContext);
    void sendRequestResult(HostAppResponse response);
    void sendErrorResponse(String requestId, Throwable error);
    void requestAuthorizationAvailability(CallbackContext callbackContext);
    void authorizationAvailabilityResult(AuthorizationAvailabilityResponse response);
    void requestAuthorization(CallbackContext callbackContext);
    void authorizationResult(AuthorizationResponse response);
    void requestNotification(CallbackContext callbackContext);
    void requestTotalNotificationsUnread(CallbackContext callbackContext);
}
